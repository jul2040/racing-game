tool
extends "res://maps/Room.gd"

func _setup():
	custom_safe_function = true
	for id in players.keys():
		players[id].connect("car_hit", self, "car_hit")
		tagged[id] = false
	Overlay.safe_count(ceil(len(players)/2.0))
	if(GameManager.is_server()):
		var safe = players.keys()
		safe.shuffle()
		for i in range(int(len(players)/2.0)):
			tagged[safe[i]] = true
		rpc("sync_tagged", tagged)

func _car_setup(c:Node):
	c.connect("car_hit", self, "car_hit")

func car_hit(car1, car2):
	if(GameManager.is_server() and tagged[car1.id] != tagged[car2.id]):
		tagged[car1.id] = not tagged[car1.id]
		tagged[car2.id] = not tagged[car2.id]
		rpc("sync_tagged", tagged)

var tagged = {}
sync func sync_tagged(t:Dictionary):
	tagged = t
	for id in tagged.keys():
		players[id].set_tagged(tagged[id])

func get_safe()->Array:
	var safe = []
	for id in tagged.keys():
		if(not tagged[id]):
			safe.append(id)
	return safe

func get_unsafe()->Array:
	var unsafe = []
	for id in tagged.keys():
		if(tagged[id]):
			unsafe.append(id)
	return unsafe
