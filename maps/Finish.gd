extends Area

var finished = false
func _on_Finish_body_entered(body):
	if(not finished and body.is_in_group("car") and not body.dummy):
		finished = true
		body.finish()
