extends ColorRect

var info = [
	["Teeny", "The world's smallest car.\n\nCan jump."],
	["Enforcer", "A literal swine.\n\nCan temporarily disable another car."],
	["Texan", "Compensating for something.\n\nCan winch other cars and itself."],
	["Dragster", "Very fast.\n\nCan use nitrous."],
	["Hippie", "Smokes lots of grass.\n\nCan repel other cars."]]

func set_car(c):
	$NameLabel.text = info[c][0]
	$InfoLabel.text = info[c][1]
