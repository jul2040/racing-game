extends ColorRect

const TURN_TIME = 0.5
var paint_mat = [preload("res://assets/cars/paintRed.material"),
		preload("res://assets/cars/paintGreen.material"),
		preload("res://assets/cars/paintWhite.material"),
		preload("res://assets/cars/paintYellow.material"),
		preload("res://assets/cars/paintBlue.material")]
var highlight_mat = preload("res://assets/cars/plastic.material")
var window_mat = preload("res://assets/cars/window.material")

func set_paint_color(c):
	for m in paint_mat:
		m.albedo_color = c

func set_highlight_color(c):
	highlight_mat.albedo_color = c

func set_tint_color(c):
	window_mat.albedo_color = c

func _ready():
	get_node("../ViewportContainer/Viewport/CameraHub").rotation.y = get_angle($CarSelector.selected_ind)
	$PaintSelector.select(GameSaver.get("paint color"))
	$HighlightSelector.select(GameSaver.get("highlight color"))
	$TintSelector.select(GameSaver.get("tint color"))
	$CarSelector.select(GameSaver.get("car"))
	$NameEdit.text = GameSaver.get("name")
	_on_PaintSelector_selected_updated(false)
	_on_HighlightSelector_selected_updated(false)
	_on_CarSelector_selected_updated(false)
	_on_TintSelector_selected_updated(false)

func _on_CarSelector_selected_updated(_dir):
	$CarInfo.set_car($CarSelector.selected_ind)
	var cur = get_node("../ViewportContainer/Viewport/CameraHub").rotation
	var to = cur
	to.y = get_angle($CarSelector.selected_ind)
	while(abs(to.y-cur.y)>PI):
		to.y += sign(cur.y-to.y)*PI*2
	$Tween.interpolate_property(get_node("../ViewportContainer/Viewport/CameraHub"), "rotation", cur, to, TURN_TIME, Tween.TRANS_BACK, Tween.EASE_OUT)
	$Tween.start()
	GameSaver.set("car", $CarSelector.selected_ind)

func get_angle(i:int)->float:
	var l = len($CarSelector.options)
	return ((l-i)*(2*PI/l))-PI/8

func short_angle_dist(from: float, to: float) -> float:
	var difference = fmod(to - from, PI * 2)
	return fmod(2 * difference, PI * 2) - difference

func _on_PaintSelector_selected_updated(_dir):
	set_paint_color(Networking.colors[$PaintSelector.selected_ind])
	GameSaver.set("paint color", $PaintSelector.selected_ind)

func _on_HighlightSelector_selected_updated(_dir):
	set_highlight_color(Networking.colors[$HighlightSelector.selected_ind])
	GameSaver.set("highlight color", $HighlightSelector.selected_ind)

func _on_TintSelector_selected_updated(_dir):
	set_tint_color(Networking.tint_colors[$TintSelector.selected_ind])
	GameSaver.set("tint color", $TintSelector.selected_ind)

func _on_LineEdit_text_changed(new_text):
	GameSaver.set("name", new_text.to_upper())

func _on_CarSelector_focus_entered():
	$CarInfo.visible = true

func _on_CarSelector_focus_exited():
	$CarInfo.visible = false
