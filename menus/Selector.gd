extends Button

var selected_ind:int = 0
export(Array) var options = ["1", "2","3"]


func loop_selected():
	while(selected_ind<0):
		selected_ind += len(options)
	while(selected_ind>len(options)-1):
		selected_ind -= len(options)

signal selected_updated(dir)
func _on_Left_focus_entered():
	select(selected_ind-1)
	grab_focus()
	emit_signal("selected_updated", false)

func _on_Right_focus_entered():
	select(selected_ind+1)
	grab_focus()
	emit_signal("selected_updated", true)

func select(i):
	selected_ind = i
	loop_selected()
	update_text()

func update_text():
	text = options[selected_ind]

func _on_Selector_pressed():
	_on_Right_focus_entered()
