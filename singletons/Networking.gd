extends Node

func _ready():
	get_tree().connect("network_peer_connected", self, "_player_connected")
	get_tree().connect("network_peer_disconnected", self, "_player_disconnected")
	get_tree().connect("connected_to_server", self, "_connected_ok")
	get_tree().connect("connection_failed", self, "_connected_fail")
	get_tree().connect("server_disconnected", self, "_server_disconnected")

const PORT:int = 5873
const MAX_PLAYERS:int = 8
var peer
func client(ip:String):
	quit()
	peer = NetworkedMultiplayerENet.new()
	var err = peer.create_client(ip, PORT)
	if(err == OK):
		get_tree().network_peer = peer
	return err

signal server_result(err)
var upnp:UPNP
var server_thread:Thread
func server():
	quit()
	#use a thread so that the blocking upnp functions dont block the main thread
	if(server_thread != null):
		server_thread.wait_to_finish()
	server_thread = Thread.new()
	server_thread.start(self, "server_process")

func _exit_tree():
	if(server_thread != null):
		server_thread.wait_to_finish()

func server_process(_arg):
	#use upnp to open the port for the server
	upnp = UPNP.new()
	var err:int = upnp.discover(2000, 2, "InternetGatewayDevice")
	if(err != 0):
		emit_signal("server_result", err)
		return
	err = upnp.add_port_mapping(PORT)
	if(err != 0):
		emit_signal("server_result", err)
		return
	peer = NetworkedMultiplayerENet.new()
	err = peer.create_server(PORT, MAX_PLAYERS)
	if(err == OK):
		get_tree().network_peer = peer
	emit_signal("server_result", err)

func allow_join(j:bool):
	get_tree().set_refuse_new_network_connections(not j)

func update_my_info():
	my_info["color"] = GameSaver.get("paint color")
	my_info["highlight color"] = GameSaver.get("highlight color")
	my_info["tint color"] = GameSaver.get("tint color")
	my_info["car"] = GameSaver.get("car")
	my_info["name"] = GameSaver.get("name")
	my_info["dead"] = false

signal new_player(id)
var player_info:Dictionary = {}
remote func register_player(info:Dictionary):
	var id:int = get_tree().get_rpc_sender_id()
	player_info[id] = info
	emit_signal("new_player", id)

var my_info:Dictionary = {}
func _player_connected(id):
	#called on server and clients when a player connects
	rpc_id(id, "register_player", my_info)

signal remove_player(id)
func _player_disconnected(id):
	player_info.erase(id)
	emit_signal("remove_player", id)

signal connected_result(result)
func _connected_ok():
	emit_signal("connected_result", true)

func _connected_fail():
	emit_signal("connected_result", false)

var error = ""
func _server_disconnected():
	error = "Lost connection to server."
	Loader.goto_scene("res://menus/MainMenu.tscn")

func kick(id:int):
	rpc_id(id, "recv_kick")

remote func recv_kick():
	quit()
	Loader.goto_scene("res://menus/MainMenu.tscn")

func kill(id:int):
	rpc("recv_kill", id)

sync func recv_kill(id:int):
	if(id == get_tree().get_network_unique_id()):
		my_info["dead"] = true
	else:
		player_info[id]["dead"] = true

func is_dead(id:int):
	if(id == get_tree().get_network_unique_id()):
		return my_info["dead"]
	else:
		return player_info[id]["dead"]

func quit():
	Matchmaking.unregister_server()
	get_tree().network_peer = null
	player_info = {}
	if(upnp != null):
		upnp.delete_port_mapping(PORT)
		upnp = null

var colors = [
		Color(1, 0, 0), #red
		Color(1, 0.585938, 0), #orange
		Color(1, 1, 0), #yellow
		Color(0.07892, 0.671875, 0.044617), #green
		Color(0, 0.765625, 1), #light blue
		Color(0.007812, 0, 1), #dark blue
		Color(0.412903, 0, 0.644531), #purple
		Color(0.734385, 0.379272, 0.933594), #magenta
		Color(0.972656, 0.653503, 0.892868), #pink
		Color(0.972656, 0.972656, 0.972656), #white
		Color(0.15625, 0.15625, 0.15625), #black
		Color(0.523438, 0.523438, 0.523438), #grey
		Color(0.515625, 0.365506, 0.153076) #brown
]

var tint_colors = [
	Color(1, 1, 1), # white
	Color(0.703125, 0.703125, 0.703125), # grey 1
	Color(0.324219, 0.324219, 0.324219), # grey 2
	Color(0, 0, 0), # black
	Color(0.402344, 0.320078, 0.191742), # brown
	Color(1, 0, 0), # red
	Color(1, 0.585937, 0), # orange
	Color(1, 0.9375, 0), # yellow
	Color(0.185425, 0.847656, 0), # green
	Color(0.039108, 0.828496, 0.910156), # light blue
	Color(0, 0.013245, 0.847656), # dark blue
	Color(0.59375, 0, 1), # purple
	Color(0.734385, 0.379272, 0.933594), #magenta
	Color(0.982055, 0.671875, 1) # pink
]
