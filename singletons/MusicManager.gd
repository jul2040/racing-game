extends Node

var tracks: = [preload("res://assets/music/dropout.wav"),
	preload("res://assets/music/fast.mp3"),
	preload("res://assets/music/badass.mp3")]
var prev_ind:int = -1
func music(ind:int):
	if(ind < 0):
		$MusicPlayer.stop()
		return
	if(ind == prev_ind):
		return
	$MusicPlayer.stream = tracks[ind]
	$MusicPlayer.play()
	prev_ind = ind
