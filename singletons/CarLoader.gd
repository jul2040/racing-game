extends Node

var small_car: = preload("res://cars/SmallCar.tscn")
var police_car: = preload("res://cars/PoliceCar.tscn")
var truck: = preload("res://cars/Truck.tscn")
var racecar: = preload("res://cars/Racecar.tscn")
var van: = preload("res://cars/Van.tscn")
