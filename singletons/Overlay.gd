extends CanvasLayer

var actions: = [
	["up", "Forwards"],
	["down", "Backwards"],
	["left", "Left"],
	["right", "Right"],
	["special", "Special Ability"],
	["drift", "Drift"],
	["interact", "Interact"],
	["horn", "Honk Horn"],
	["pause", "Pause"],
	["zoom_in", "Zoom Camera In"],
	["zoom_out", "Zoom Camera Out"],
	["spectate_next", "Spectate Next"],
	["spectate_prev", "Spectate Previous"]
]
var control_buttons: = []
var resolutions: = [200, 480, 720, 1080, 1440, 2160]
func _ready():
	Networking.connect("new_player", self, "update_players")
	Networking.connect("remove_player", self, "update_players")
	if(not GameSaver.setup_done):
		yield(GameSaver, "setup")
	$Ingame/PauseMenu/FovSlider.value = GameSaver.get("fov")
	for r in resolutions:
		$Settings/ResolutionButton.add_item(str(r)+"p")
	$Settings/ResolutionButton.select(GameSaver.get("resolution"))
	_on_ResolutionButton_item_selected(GameSaver.get("resolution"))
	$Settings/FullscreenButton.pressed = (GameSaver.get("fullscreen"))
	_on_FullscreenButton_toggled(GameSaver.get("fullscreen"))
	$Settings/MouseSensitivity.value = GameSaver.get("sensitivity")
	_on_MouseSensitivity_value_changed(GameSaver.get("sensitivity"))
	$Ingame/PauseMenu/RotateCarButton.pressed = GameSaver.get("rotate camera")
	_on_RotateCarButton_toggled(GameSaver.get("rotate camera"))
	for a in actions:
		#make menu item
		var s = preload("res://menus/ControlSetting.tscn").instance()
		s.set_action(a)
		s.connect("reassign", self, "reassign_pressed")
		$Settings/Controls/ScrollContainer/VBoxContainer.add_child(s)
		control_buttons.append(s)
	control_buttons[0].set_up($Settings/Controls/ExitControls.get_path())
	$Settings/Controls/ExitControls.focus_neighbour_bottom = control_buttons[0].get_path()
	$Settings/Controls/ExitControls.focus_neighbour_top = control_buttons[len(control_buttons)-1].get_path()
	control_buttons[len(control_buttons)-1].set_bottom($Settings/Controls/ExitControls.get_path())
	$WaitingRoom/StartLabel.text = "PRESS %s TO START"%get_control("interact")

func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_FOCUS_OUT:
		if($Ingame.visible and not $Ingame/PauseMenu.visible):
			pause(true)

func highlight(h:bool):
	if(h):
		$Ingame/Crosshair.modulate.a = 0.75
	else:
		$Ingame/Crosshair.modulate.a = 0.1

func ingame(i:bool):
	$Ingame.visible = i

func disabled(d:bool):
	$Ingame/DisabledIndicator.visible = d

func _on_ExitPause_pressed():
	$Ingame/PauseMenu.visible = false
	emit_signal("pause", $Ingame/PauseMenu.visible)

signal fov_changed(v)
func _on_FovSlider_value_changed(value):
	emit_signal("fov_changed", value)
	GameSaver.set("fov", value)

func get_fov()->float:
	return $Ingame/PauseMenu/FovSlider.value

signal pause(p)
func _input(event):
	if(waiting_for_input and (event is InputEventKey or event is InputEventMouseButton)):
		setting.assign_button(event)
		if(setting.action == "interact"):
			$WaitingRoom/StartLabel.text = "PRESS %s TO START"%get_control("interact")
		setting = null
		waiting_for_input = false
		return
	if(Input.is_action_just_pressed("pause")):
		if($Ingame.visible):
			if(not $Settings.visible):
				pause(not $Ingame/PauseMenu.visible)
		if($Settings/Controls.visible):
			controls(false)
		elif($Settings.visible):
			settings(false)
	if($WaitingRoom/StartLabel.visible and Input.is_action_just_pressed("interact")):
		Matchmaking.unregister_server()
		GameManager.start_game()

func pause(p:bool):
	if($Scoreboard.visible):
		return
	$Ingame/PauseMenu.visible = p
	emit_signal("pause", p)
	if(p):
		$Ingame/PauseMenu/ExitPause.grab_focus()

func _on_MainMenu_pressed():
	Networking.quit()
	ingame(false)
	$Ingame/PauseMenu.visible = false
	Loader.goto_scene("res://menus/MainMenu.tscn")

signal settings_closed
func settings(s:bool):
	$Settings.visible = s
	if(s):
		$Settings/SettingsExit.grab_focus()
	else:
		emit_signal("settings_closed")
		if($Ingame.visible):
			$Ingame/PauseMenu/Settings.grab_focus()

signal resolution_changed
func _on_ResolutionButton_item_selected(index):
	emit_signal("resolution_changed")
	GameSaver.set("resolution", index)

func get_resolution()->int:
	return resolutions[$Settings/ResolutionButton.selected]

func _on_FullscreenButton_toggled(button_pressed):
	OS.window_fullscreen = button_pressed
	GameSaver.set("fullscreen", button_pressed)

func _on_SettingsExit_pressed():
	settings(false)

func _on_Settings_pressed():
	settings(true)

func waiting_room(w:bool):
	update_players(-1)
	$WaitingRoom.visible = w
	$WaitingRoom/StartLabel.visible = w and GameManager.is_server()

func update_players(_id):
	var players = Networking.player_info.size()+1
	$WaitingRoom/PlayerLabel.text = "PLAYERS (%d/%d)"%[players, Networking.MAX_PLAYERS]
	$WaitingRoom/StartLabel.visible = players >= GameManager.MINIMUM_PLAYERS and GameManager.is_server()

func scoreboard(scores, cont:bool):
	$Scoreboard.visible = true
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	$Scoreboard.show_scores(scores, cont)

func close_scoreboard():
	$Scoreboard.visible = false

func timer(t:bool):
	$Ingame/TimerLabel.visible = t

func time_to_string(seconds:float):
	return "%02d:%02d:%02d"%[seconds/60, wrapf(seconds, 0, 60), wrapf(seconds*100, 0, 100)]

func set_time(t:float):
	$Ingame/TimerLabel.text = time_to_string(t)

func safe(s:bool):
	$Ingame/Safe.visible = s

func safe_count(c:int):
	$Ingame/Safe/SafeLabel.text = "%d/%d"%[c, GameManager.safe_count]

func start_timer(t:bool):
	$Ingame/StartTimerLabel.visible = t
	$Ingame/GameNameLabel.visible = t
	$Ingame/GameDescLabel.visible = t

func set_start_timer(t:String):
	$Ingame/StartTimerLabel.text = t

func game_name(n:String):
	$Ingame/GameNameLabel.text = n

func game_desc(d:String):
	$Ingame/GameDescLabel.text = d

func controls(c:bool):
	$Settings/Controls.visible = c
	if(not c):
		waiting_for_input = false
		if(setting != null):
			setting.not_waiting()
			setting = null

var waiting_for_input = false
var setting
func reassign_pressed(s):
	waiting_for_input = true
	s.waiting()
	if(setting != null):
		setting.not_waiting()
	setting = s

func _on_ExitControls_pressed():
	controls(false)
	$Settings/ControlsButton.grab_focus()

func _on_ControlsButton_pressed():
	controls(true)
	$Settings/Controls/ExitControls.grab_focus()

func _on_ResetControlsDefaults_pressed():
	for c in control_buttons:
		c.reset()

func get_control(control:String)->String:
	var event = event_from_dict(GameSaver.get("keybinds")[control])
	return event_to_string(event)

func event_from_dict(d:Dictionary)->InputEvent:
	if(d["type"] == "key"):
		var e = InputEventKey.new()
		e.set_scancode(d["scancode"])
		return e
	if(d["type"] == "Mouse Button"):
		var e = InputEventMouseButton.new()
		e.set_button_index(d["index"])
		return e
	return null

func event_to_string(event:InputEvent)->String:
	if(event is InputEventKey):
		return OS.get_scancode_string(event.get_scancode())
	elif(event is InputEventMouseButton):
		return "mouse button "+str(event.button_index)
	return ""

signal sensitivity_changed(v)
func _on_MouseSensitivity_value_changed(value):
	GameSaver.set("sensitivity", value)
	emit_signal("sensitivity_changed", value)

func fade_in():
	$FadeRect/AnimationPlayer.play("fade_in")

signal rotate_camera(r)
func _on_RotateCarButton_toggled(button_pressed):
	GameSaver.set("rotate camera", button_pressed)
	emit_signal("rotate_camera", button_pressed)
