extends Camera

export(NodePath) var car_path:NodePath
export(float) var hide_distance:float = 5

var car:Node
func _ready():
	Overlay.connect("fov_changed", self, "fov_changed")
	fov_changed(Overlay.get_fov())
	car = get_node(car_path)
	hide_distance = hide_distance*hide_distance # for squared distance comparison

func _physics_process(_delta:float):
	if(dummy):
		return
	car.set_hidden(car.global_transform.origin.distance_squared_to(global_transform.origin) < hide_distance)

var dummy = false
func set_dummy(d:bool):
	dummy = d

func fov_changed(new:float):
	fov = new
