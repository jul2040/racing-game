extends "res://cars/Car.gd"

export(float) var nitro_torque = 1000

var normal_torque:float
func _setup():
	normal_torque = ENGINE_TORQUE

func _special_just_pressed():
	rpc("sync_nitro")
	use_special()

sync func sync_nitro():
	ENGINE_TORQUE = nitro_torque
	$Chassis/NitroParticles.emitting = true
	$NitroPlayer.play()
	$NitroTimer.start()

func _on_NitroTimer_timeout():
	ENGINE_TORQUE = normal_torque
	$Chassis/NitroParticles.emitting = false

func _special_just_released():
	pass

func _special_pressed(_delta:float):
	pass

func _special_process(_delta:float):
	Overlay.highlight(can_special)

func _set_highlight_color(c:Color):
	$FR/MeshInstance.get_active_material(2).albedo_color = c
	$FL/MeshInstance.get_active_material(2).albedo_color = c
	$BR/MeshInstance.get_active_material(2).albedo_color = c
	$BL/MeshInstance.get_active_material(2).albedo_color = c
