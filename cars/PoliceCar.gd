extends "res://cars/Car.gd"

onready var ray:RayCast = $Node/ViewportContainer/Viewport/CameraHub/Camera/EmpRay
onready var indicator: = $EmpIndicator

export(float) var max_emp_dist := 10.0
var charging := false
var emp_object:RigidBody

func _setup():
	ray = $Node/ViewportContainer/Viewport/CameraHub/Camera/EmpRay
	ray.add_exception(self)
	max_emp_dist = max_emp_dist*max_emp_dist # for squared distance comparison

func _special_just_pressed():
	if(ray.is_colliding()):
		if(ray.get_collision_point().distance_squared_to(global_transform.origin) < max_emp_dist):
			var col := ray.get_collider()
			if(col.is_in_group("car")):
				emp_object = col
				set_charging(true)
				rpc("sync_charging", charging, emp_object.get_path())
			else:
				emp_object = null
				set_charging(false)
	else:
		set_charging(false)

func _special_just_released():
	set_charging(false)
	rpc("sync_charging", charging)

func set_charging(c:bool):
	charging = c
	indicator.visible = c
	if(dummy):
		return
	if(c):
		if($ChargeTimer.time_left == 0):
			$ChargeTimer.start()
	else:
		$ChargeTimer.stop()

func _special_process(_delta:float):
	if(charging):
		var emp_object_valid:bool = is_instance_valid(emp_object)
		if(emp_object_valid):
			indicator.look_at(emp_object.global_transform.origin, Vector3.UP)
			var indicator_dist:float = indicator.global_transform.origin.distance_to(emp_object.global_transform.origin)
			indicator.scale.z = indicator_dist
			
			var dist:float = global_transform.origin.distance_squared_to(emp_object.global_transform.origin)
			if(dist > max_emp_dist):
				set_charging(false) # out of range, stop charging
	if(not dummy):
		var col = ray.get_collider()
		if(col == null):
			Overlay.highlight(false)
			return
		if(not col.is_in_group("car")):
			Overlay.highlight(false)
			return
		if(not can_special):
			Overlay.highlight(false)
			return
		if(col.global_transform.origin.distance_squared_to(global_transform.origin) < max_emp_dist):
			Overlay.highlight(true)
		else:
			Overlay.highlight(false)

func _on_ChargeTimer_timeout():
	if(is_instance_valid(emp_object)):
		emp_object.emp()
		rpc("use_emp")
		use_special()

sync func use_emp():
	$EmpPlayer.playing = true

remote func sync_charging(c:bool, target:NodePath=""):
	set_charging(c)
	if(target != ""):
		emp_object = get_node(target)
	_special_process(0)
