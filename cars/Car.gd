extends VehicleBody

export(float) var BRAKE_FORCE := 200.0
export(float) var MANUAL_BRAKE_FORCE := 500.0
export(float) var ENGINE_TORQUE := 3000.0
export(float) var FRONT_TRACTION := 2.0
export(float) var BACK_TRACTION := 3.0
export(float) var DRIFT_TRACTION := 1.0
export(float) var PITCH_CONTROL := 50000.0
export(float) var ROLL_CONTROL := 50000.0
export(float) var TURNOVER_CONTROL := 100000.0
export(float) var MAX_ROLL_SPEED := 4.0

export(bool) var dummy = false setget set_dummy

func set_dummy(d):
	dummy = d
	$Node/ViewportContainer/Viewport/CameraHub/Camera.current = not dummy
	$Node/ViewportContainer/Viewport/CameraHub/Camera.set_dummy(d)
	$Node/Label3D.visible = dummy

var id:int = 1
func _ready():
	$FL.wheel_friction_slip = FRONT_TRACTION
	$FR.wheel_friction_slip = FRONT_TRACTION
	Overlay.connect("rotate_camera", self, "rotate_camera_changed")
	set_drift(false)
	Overlay.ingame(true)
	set_dummy(dummy)
	_setup()

func _setup():
	pass

func is_upside_down()->bool:
	return global_transform.basis.y.dot(Vector3.UP) < 0

var remote_actions := {"up":false,"down":false,"left":false,"right":false, "drift":false}
remote func set_action(a:String, pressed:bool):
	if(not remote_actions.has(a)):
		print("WARNING: Invalid remote action")
		return
	remote_actions[a] = pressed

remote func sync_transform(t:Transform):
	transform = t

remote func sync_velocity(lv:Vector3, av:Vector3):
	linear_velocity = lv
	angular_velocity = av

func is_pressed(action):
	if(dummy):
		return remote_actions[action]
	else:
		if(Input.get_mouse_mode() != Input.MOUSE_MODE_CAPTURED):
			return false
		return Input.is_action_pressed(action)

func get_steer_angle(speed:float):
	return clamp(1.0/(abs(speed*0.1)+1), 0.2, 0.7)

onready var wheels = [$FR, $FL, $BR, $BL]
var is_on_floor:bool = false
func _physics_process(delta):
	#calculate speed
	var speed = linear_velocity.project(global_transform.basis.z).length()
	speed *= -sign(global_transform.basis.z.dot(linear_velocity))
	
	is_on_floor = false
	var skid:float = 0
	for w in wheels:
		if(w.is_in_contact()):
			is_on_floor = true
		skid += 1-w.get_skidinfo()
	skid /= len(wheels)
	if(is_on_floor and rotate_camera):
		$Node/ViewportContainer/Viewport/CameraHub.car_rotation(rotation.y)
	else:
		$Node/ViewportContainer/Viewport/CameraHub.stop_rotation()
	#sfx
	#engine
	if(is_on_floor):
		$EnginePlayer.pitch_scale = lerp($EnginePlayer.pitch_scale, (abs(speed)*0.15)+1, delta*5)
	else:
		$EnginePlayer.pitch_scale = lerp($EnginePlayer.pitch_scale, 1, delta*5)
	
	#skid
	if(is_on_floor and linear_velocity.length_squared() > 5*5):
		$DriftPlayer.unit_db = lerp($DriftPlayer.unit_db, skid*60-40, delta*5)
	else:
		$DriftPlayer.unit_db = lerp($DriftPlayer.unit_db,-80,delta*5)
	
	#horn
	if(Input.is_action_just_pressed("horn")):
		rpc("sync_horn", true)
	if(Input.is_action_just_released("horn")):
		rpc("sync_horn", false)
	
	#calculate roll
	#positive = left roll, negative = right roll
	var roll = angular_velocity.project(global_transform.basis.z).dot(global_transform.basis.z)
	
	if(controls_enabled and is_pressed("up")):
		engine_force = -ENGINE_TORQUE
		if(speed > 0):
			brake = 0
		else:
			brake = lerp(brake, MANUAL_BRAKE_FORCE, clamp(delta, 0, 1))
	elif(controls_enabled and is_pressed("down")):
		engine_force = ENGINE_TORQUE
		if(speed < 0):
			brake = 0
		else:
			brake = lerp(brake, MANUAL_BRAKE_FORCE, clamp(delta, 0, 1))
	else:
		engine_force = 0
		brake = lerp(brake, BRAKE_FORCE, clamp(delta, 0, 1)*0.25)
	
	var steer_angle = get_steer_angle(speed)
	if(controls_enabled and is_pressed("left")):
		steering = lerp(steering, steer_angle, clamp(delta, 0, 1)*10)
		if(roll < MAX_ROLL_SPEED and is_upside_down()):
			add_torque(global_transform.basis.z*(TURNOVER_CONTROL*delta))
	elif(controls_enabled and is_pressed("right")):
		steering = lerp(steering, -steer_angle, clamp(delta, 0, 1)*10)
		if(roll > -MAX_ROLL_SPEED and is_upside_down()):
			add_torque(global_transform.basis.z*(-TURNOVER_CONTROL*delta))
	else:
		steering = lerp(steering, 0, clamp(delta, 0, 1)*10)
	
	if(controls_enabled and is_pressed("drift")):
		set_drift(true)
	else:
		set_drift(false)
	
	#pitch control
	if(controls_enabled and is_pressed("up")):
		add_torque(global_transform.basis.x*(-PITCH_CONTROL*delta))
	if(controls_enabled and is_pressed("down")):
		add_torque(global_transform.basis.x*(PITCH_CONTROL*delta))
	#roll control
	if(roll < MAX_ROLL_SPEED and controls_enabled and is_pressed("left")):
		add_torque(global_transform.basis.z*(ROLL_CONTROL*delta))
	if(roll > -MAX_ROLL_SPEED and controls_enabled and is_pressed("right")):
		add_torque(global_transform.basis.z*(-ROLL_CONTROL*delta))
	
	_special_process(delta)
	
	if(dummy):
		return
	if(Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED and can_special and controls_enabled and Input.is_action_just_pressed("special")):
		_special_just_pressed()
	if(Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED and can_special and controls_enabled and Input.is_action_just_released("special")):
		_special_just_released()
	if(Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED and can_special and controls_enabled and Input.is_action_pressed("special")):
		_special_pressed(delta)
	
	#sync
	rpc("set_action", "up", Input.is_action_pressed("up"))
	rpc("set_action", "down", Input.is_action_pressed("down"))
	rpc("set_action", "left", Input.is_action_pressed("left"))
	rpc("set_action", "right", Input.is_action_pressed("right"))
	rpc("set_action", "drift", Input.is_action_pressed("drift"))
	
	rpc_unreliable("sync_transform", transform)
	rpc_unreliable("sync_velocity", linear_velocity, angular_velocity)

func set_drift(d:bool):
	if(d):
		$BR.wheel_friction_slip = DRIFT_TRACTION
		$BL.wheel_friction_slip = DRIFT_TRACTION
	else:
		$BR.wheel_friction_slip = BACK_TRACTION
		$BL.wheel_friction_slip = BACK_TRACTION

func set_hidden(h:bool):
	visible = not h

func _special_just_pressed():
	pass

func _special_just_released():
	pass

func _special_pressed(_delta:float):
	pass

func _special_process(_delta:float):
	pass

sync func sync_horn(h:bool):
	$HornPlayer.playing = h

var controls_enabled = true
func emp():
	if(dummy):
		rpc("sync_emp")
		return
	controls_enabled = false
	Overlay.disabled(true)
	$EmpTimer.start()
	if(Input.is_action_pressed("special")):
		_special_just_released()

remote func sync_emp():
	if(dummy):
		return
	emp()

func _on_EmpTimer_timeout():
	controls_enabled = true
	Overlay.disabled(false)
	if(Input.is_action_pressed("special")):
		_special_just_pressed()

func use_special():
	$SpecialCooldown.start()
	can_special = false
	if(Input.is_action_pressed("special")):
		_special_just_released()

var can_special = true
func _on_SpecialCooldown_timeout():
	can_special = true
	if(Input.is_action_pressed("special")):
		_special_just_pressed()

export(int) var MAIN_SURFACE = 1
func set_color(c:int):
	var color:Color = Networking.colors[c]
	$Chassis.get_active_material(MAIN_SURFACE).albedo_color = color
	_set_color(color)

func _set_color(_c):
	pass

export(int) var HIGHLIGHT_SURFACE = 0
func set_highlight_color(c:int):
	var color:Color = Networking.colors[c]
	$Chassis.get_active_material(HIGHLIGHT_SURFACE).albedo_color = color
	_set_highlight_color(color)

func _set_highlight_color(_c:Color):
	pass

export(int) var WINDOW_SURFACE = 5
func set_tint_color(c:int):
	var color:Color = Networking.tint_colors[c]
	$Chassis.get_active_material(WINDOW_SURFACE).albedo_color = color

func set_nick(n):
	$Node/Label3D.set_text(n)

func set_current(c:bool):
	$Node/ViewportContainer/Viewport/CameraHub/Camera.current = c
	$Node/ViewportContainer.visible = c

func is_current():
	return $Node/ViewportContainer/Viewport/CameraHub/Camera.current

func finish():
	rpc("recv_explode")

sync func recv_explode():
	mode = RigidBody.MODE_STATIC
	$CollisionShape.disabled = true
	$ExplodeParticles.rotation = -rotation
	$ExplodeParticles.emitting = true
	$EnginePlayer.playing = false
	$ExplosionPlayer.playing = true
	$Chassis.visible = false
	$FR.visible = false
	$FL.visible = false
	$BR.visible = false
	$BL.visible = false
	$FinishTimer.start()
	get_parent().die(self)

func time_to_string(seconds:float):
	return "%02d:%02d:%02d"%[seconds/60, wrapf(seconds, 0, 60), wrapf(seconds*100, 0, 100)]

func _on_FinishTimer_timeout():
	if(not dummy):
		GameManager.finish(time_to_string(get_parent().time))
		rpc("recv_free")

sync func recv_free():
	queue_free()

signal car_hit(c1, c2)
func _on_Car_body_entered(body):
	if(can_tag and not dummy and body.is_in_group("car")):
		$TagbackTimer.start()
		can_tag = false
		emit_signal("car_hit", self, body)

func set_tagged(t:bool):
	$TaggedParticles.emitting = t

var can_tag = true
func _on_TagbackTimer_timeout():
	can_tag = true

onready var rotate_camera:bool = GameSaver.get("rotate camera")
func rotate_camera_changed(c:bool):
	rotate_camera = c
