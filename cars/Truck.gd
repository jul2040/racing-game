extends "res://cars/Car.gd"

export(float) var WINCH_STRENGTH := 1000000

var winch_ray:RayCast
onready var winch_pos:Position3D = $WinchPos

export(float) var max_winch_dist := 10.0
var winch_to := Vector3()
var winching := false
var winched_object:RigidBody

func _setup():
	winch_ray = $Node/ViewportContainer/Viewport/CameraHub/Camera/WinchRay
	winch_ray.add_exception(self)
	max_winch_dist = max_winch_dist*max_winch_dist # for squared distance comparison

func _special_just_pressed():
	if(winch_ray.is_colliding()):
		winch_to = winch_ray.get_collision_point()
		if(winch_to.distance_squared_to(global_transform.origin) < max_winch_dist):
			set_winching(true)
			var col = winch_ray.get_collider()
			$GrapplePlayer.playing = true
			if(col.is_in_group("winchable")):
				winched_object = col
				rpc("sync_winching", winching, winch_to, winched_object.get_path())
			else:
				winched_object = null
				rpc("sync_winching", winching, winch_to)
	else:
		set_winching(false)

func _special_just_released():
	if(winching):
		use_special()
	set_winching(false)
	rpc("sync_winching", winching)

func set_winching(w:bool):
	winching = w
	winch_pos.visible = winching

func _special_pressed(_delta:float):
	pass

func _special_process(delta:float):
	if(not dummy):
		if winching or (can_special and winch_ray.is_colliding() and not winching and winch_ray.get_collision_point().distance_squared_to(global_transform.origin) < max_winch_dist):
			Overlay.highlight(true)
		else:
			Overlay.highlight(false)
	if(not winching):
		return
	
	var winched_object_valid:bool = is_instance_valid(winched_object)
	if(winched_object_valid):
		winch_to = winched_object.global_transform.origin
	
	winch_pos.look_at(winch_to, Vector3.UP)
	var dist:float = winch_pos.global_transform.origin.distance_to(winch_to)
	winch_pos.scale.z = dist
	
	if(dist > 5):
		BRAKE_FORCE = 0
		var direction:Vector3 = (winch_to-global_transform.origin).normalized()
		add_force(direction*(WINCH_STRENGTH*delta), -(global_transform.origin-winch_pos.global_transform.origin))
		if(winched_object_valid):
			winched_object.add_force(-direction*(WINCH_STRENGTH*delta), Vector3())
	else:
		BRAKE_FORCE = 200

remote func sync_winching(w:bool, pos:Vector3=Vector3(), obj:NodePath=""):
	set_winching(w)
	if(obj != ""):
		winched_object = get_node(obj)
	else:
		winched_object = null
	winch_to = pos
	_special_process(0)
	if(w):
		$GrapplePlayer.playing = true
